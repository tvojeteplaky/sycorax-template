<?php
  session_start();
  require("./form.php");

  $form = new Form();
  $state = false;

  if(!empty($_POST["sycorax_form"]) && $form->checkFormHash($_POST["sycorax_form"])) {
    $state = $form->sendMail($_POST["form"]);
  }
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sycorax a.s.</title>
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>
    <div id="page" class="index">
      <header>
        <div class="row">
          <div class="small-2 columns logo1">
            <img src="img/sycoraxLogo.png" alt="">
          </div>
          <div class="small-2 columns logo2">
            <img src="img/sigfoxLogo.png" alt="">
          </div>
          <div class="small-2 small-offset-6 columns text-right">
            <a href="#" class="button" id="kontakt">Kontaktujte nás</a>
          </div>
        </div>
      </header>

      <section>
        <article>
          <div class="row">
            <div class="small-12 columns">
              <h1>
                <strong>Sycorax</strong> je společnost, která se <strong>specializuje</strong>
                <br>
                na <strong>řešení aplikací sítí IoT</strong>
              </h1>
            </div>
          </div>
          <div class="row large-up-3 medium-up-2 small-up-1 boxes">
            <div class="column">
              <div class="box" style="background-image: url('img/box/box1.png')">
                <h2>Smart City</h2>
                <p>
                  Lorem ipsum dolor sit amet, dolor sit amet
                  dolor sit amet...
                </p>
              </div>
            </div>
            <div class="column">
              <div class="box" style="background-image: url('img/box/box2.png')">
                <h2>Utility</h2>
                <p>
                  Lorem ipsum dolor sit amet, dolor sit amet
                  dolor sit amet...
                </p>
              </div>
            </div>
            <div class="column">
              <div class="box" style="background-image: url('img/box/box3.png')">
                <h2>Industry</h2>
                <p>
                  Lorem ipsum dolor sit amet, dolor sit amet
                  dolor sit amet...
                </p>
              </div>
            </div>
            <div class="column">
              <div class="box" style="background-image: url('img/box/box4.png')">
                <h2>Facility managment</h2>
                <p>
                  Lorem ipsum dolor sit amet, dolor sit amet
                  dolor sit amet...
                </p>
              </div>
            </div>
            <div class="column">
              <div class="box" style="background-image: url('img/box/box5.png')">
                <h2>Transport</h2>
                <p>
                  Lorem ipsum dolor sit amet, dolor sit amet
                  dolor sit amet...
                </p>
              </div>
            </div>
            <div class="column">
              <div class="box" style="background-image: url('img/box/box6.png')">
                <h2>Agriculture</h2>
                <p>
                  Lorem ipsum dolor sit amet, dolor sit amet
                  dolor sit amet...
                </p>
              </div>
            </div>
          </div>
        </article>
      </section>
    </div>
    <footer>
      <div class="row">
        <div class="small-12 columns">
          <h2>Kontaktujte nás</h2>
        </div>
      </div>
      <div class="row">
        <div class="large-7 columns">
          <form action="?" method="post">
            <div class="row smallGutter" data-equalizer>
              <div class="medium-5 columns" data-equalizer-watch>
                <div class="row">
                  <div class="small-12 columns">
                    <label>
                      <input type="text" placeholder="Jméno a příjmení" name="form[jmeno]">
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="small-12 columns">
                    <label>
                      <input type="tel" placeholder="Telefonní číslo" name="form[tel]">
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="small-12 columns">
                    <label>
                      <input type="email" placeholder="Emailová adresa" name="form[email]">
                    </label>
                  </div>
                </div>
              </div>
              <div class="medium-7 columns textarea" data-equalizer-watch>
                <label>
                  <textarea placeholder="Text zprávy" name="form[zprava]"></textarea>
                </label>
              </div>
            </div>
            <div class="row">
              <div class="large-8 columns danke" <?php if(!$state):?>style="display:none;"<?endif;?>>
                <img src="img/i.png" alt="">Děkujeme Vám za zprávu, ozveme se!
              </div>
              <div class="large-4 columns text-right">
                <input type="text" value="<?=$form->getFormHash();?>" name="sycorax_form" style="display:none;">
                <button type="submit" class="button">Odeslat</button>
              </div>
            </div>
          </form>
        </div>
        <div class="large-5 columns">
          <p>
            <b>Speciání společnost, s. r. o.,</b>
            <br>
            Speciální Adresa 585/25,
            <br>
            466 02 Super Město
          </p>
          <p>
            +420 123 456 789
            <br>
            info@domain.com
          </p>
        </div>
      </div>
    </footer>

    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
