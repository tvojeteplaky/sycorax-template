<?php
class Form {
	private $hash;
	private $to = "ondrej.brabec@dgstudio.cz";
	private $subject = "Zpráva z kontaktního formuláře webu sycorax.cz";
	public function __construct() {
		$hash = substr(md5(rand(1,100)),1,15);
		$this->hash = $hash;
	}

	public function getFormHash() {
		$_SESSION["sycorax_form"]=$this->hash;
		return $this->hash;
	}

	public function checkFormHash($hash) {
		return $_SESSION["sycorax_form"] == $hash;
	}

	public function sendMail($data) {
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <info@dgstudio.cz>' . "\r\n";
		return mail($this->to,$this->subject,$this->generateMail($data),$headers);
	}

	private function generateMail($data) {
		return '<table cellpadding="5" style="border: none; border-collapse: collapse;"><tr><td><strong>Zpráva z kontaktního formuláře www stránek</strong></td><td>sycorax.cz</td></tr><tr><td><strong>Název stránky</strong></td><td>Sycorax</td></tr><tr><td><strong>Jméno a příjmení</strong></td><td>'.$data['jmeno'].'</td></tr><tr><td><strong>E-mail odesílatele</strong></td><td>'.$data['email'].'</td></tr><tr><td><strong>Telefon</strong></td><td>'.$data['tel'].'</td></tr><tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">Text dotazu:</td></tr><tr><td colspan="2">'.$data['zprava'].'</td></tr></table>';
	}
}